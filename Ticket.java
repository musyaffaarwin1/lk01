public class Ticket {
    // Tulis kode disini

    private String Nama;
    private String Asal;
    private String Tujuan;

    //Konstruktor untuk tiket yang hanya terdapat nama saja
    public Ticket(String nama){
        this.Nama = nama;
    }

    //Konstruktor untuk tiket yang terdapat nama, asal, serta tujuan
    public Ticket(String nama, String asal, String tujuan){
        this.Nama = nama;
        this.Asal = asal;
        this.Tujuan = tujuan;
    }

    //Method untuk menampilkan data tiket
    public void printTicket(){

        //Menggunakan if untuk mengecek jenis tiket, apakah hanya terdapat nama atau terdapat asal serta tujuannya juga.
        if(Asal == null) {
            System.out.println("Nama: " +this.Nama);
        }
        else {
            System.out.println("Nama: " +this.Nama);
            System.out.println("Asal: " +this.Asal);
            System.out.println("Tujuan: " +this.Tujuan);
            System.out.println("--------------------------");
        }   
    }
}
