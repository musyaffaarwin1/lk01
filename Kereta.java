public class Kereta {
    
    // Tulis kode disini
    private String NamaKereta;
    private int jumlahTiket;
    private Ticket[] ticket;

    //Konstruktor untuk kereta yang nama dan jumlah tiket tidak dituliskan
    public Kereta(){
        this.NamaKereta = "Komuter";
        this.jumlahTiket = 1000;
        ticket = new Ticket[this.jumlahTiket];
    }

    //Konstruktor untuk kereta yang nama dan jumlah tiket dituliskan
    public Kereta(String namaKereta, int jumlahTiket){
        this.NamaKereta = namaKereta;
        this.jumlahTiket = jumlahTiket;
        ticket = new Ticket[this.jumlahTiket];
    }

    //Konstruktor untuk menambahkan tiket yang hanya menggunakan nama
    public void tambahTiket(String nama){

        //menggunakan if untuk mengecek apakah tiket masih tersedia
        if (jumlahTiket > 0) {
            
            //mengurangi jumlah tiket setelah tiket dipesan
            this.jumlahTiket--;

            //Menggunakan for untuk mengisi data tiket yang berupa ArrayList
            for (int i = 0; i < ticket.length; i++) {

                // Kode untuk mengecek dalam ArrayList terdapat data/tidak
                // Jika sudah terdapat data, maka data akan diisikan dalam ArrayList selanjutnya
               
                if (ticket[i] != null) {
                    continue;
                } 
                else {
                    ticket[i] = new Ticket(nama);
                    break;
                }
            }
            System.out.println("================================================================");
            System.out.print("Tiket berhasil dipesan");

            //Kode Menggunakan if untuk menampilkan sisa tiket ketika jumlah tiket > 30
            if (jumlahTiket < 30 && jumlahTiket >= 0) {
                System.out.print(". Jumlah tiket teresdia: " +jumlahTiket);
            }
            System.out.println();
        }
        else {
            System.out.println("================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    //Konstruktor untuk menambahkan tiket yang menggunakan nama, asal, dan tujuan
    public void tambahTiket(String nama, String asal, String tujuan){

        // Kode menggunakan if untuk mengecek apakah tiket masih tersedia
        if (jumlahTiket > 0) {

            //mengurangi jumlah tiket setelah tiket dipesan
            this.jumlahTiket--;

            //Menggunakan for untuk mengisi data tiket yang berupa ArrayList
            for (int i = 0; i < ticket.length; i++) {

                // Kode untuk mengecek dalam ArrayList terdapat data/tidak
                // Jika sudah terdapat data, maka data akan diisikan dalam ArrayList selanjutnya

                if (ticket[i] != null) {
                    continue;
                } 
                else {
                    ticket[i] = new Ticket(nama, asal, tujuan);
                    break;
                }
            }

            System.out.println("================================================================");
            System.out.print("Tiket berhasil dipesan");

            //Kode menggunakan if untuk menampilkan sisa tiket ketika jumlah tiket > 30
            if (jumlahTiket < 30 && jumlahTiket >= 0) {
                System.out.print(". Jumlah tiket teresdia: " +jumlahTiket);
            }
            System.out.println();
        }
        else {
            System.out.println("================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    //Method untuk menampilkan data kereta api
    public void tampilkanTiket(){

        //Menampilkan Nama Kereta
        System.out.println("================================================================");
        System.out.println("Daftar penumpang kereta api " +this.NamaKereta +":");
        System.out.println("--------------------------");

        //Menggunakan for untuk menampilkan seluruh data penumpang berdasarkan tiket
        for (Ticket tiket : ticket) {  

            /*
            Menggunakan if untuk menampilkan data seluruh penumpang.

            Karena nilai dari tiket yang belum dipesan akan menjadi kosong.
            Ketika proses print akan mencapai pada nilai kosong tersebut, proses print akan di hentikan.
            Sehingga tiket yang akan ditampilkan hanya berjumlah sebanyak tiket yang sudah dipesan.
            */
            if (tiket == null) {
                break;
            } 
            else {
                tiket.printTicket();
            }
        }

    }

}
